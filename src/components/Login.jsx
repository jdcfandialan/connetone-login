import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {GoogleLogin} from 'react-google-login';
import {useHistory} from 'react-router-dom';
import UserContext from '../contexts/userContext';
import {CreateUser} from './CreateUser';

export const Login = () => {
    const {setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoginActive, setIsLoginActive] = useState(false);
    let history = useHistory();

    useEffect(() => {
        if(email !== '' && password !== '' && password.length > 7) return setIsLoginActive(true);

        else setIsLoginActive(false);
    }, [email, password])

    const authenticate = e => {
        e.preventDefault();

        fetch('https://aqueous-shelf-56933.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken);
                fetch('https://aqueous-shelf-56933.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                }).then(res => res.json())
                .then(data => {
                    setUser({
                        id: data._id,
                        firstName: data.firstName
                    });

                    history.push('/home');
                })
            }

            else alert("Login Error!")
        })
    }

    const captureLoginResponse = response => {
        fetch('https://aqueous-shelf-56933.herokuapp.com/api/users/verifyGoogleIdToken', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }).then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken);

                setUser({
                    id: data._id,
                    firstName: data.firstName
                });

                history.push('/home');
            }

            else alert("Google Login Error!")
        })
    }

    return (
        <div className="login-form-div">
            <h2 className="text-center font-weight-bold pt-5 pb-3">Login</h2>
            
            <Form className="px-5" onSubmit = {e => authenticate(e)}>
                <Form.Group className="py-1">
                    <Form.Label className="pl-2">Email</Form.Label>

                    <Form.Control className="rounded-pill" placeholder="Type your email" type="email" name="email" value={email} onChange={e => setEmail(e.target.value)} />
                </Form.Group>

                <Form.Group className="py-1">
                    <Form.Label className="pl-2">Password</Form.Label>

                    <Form.Control className="rounded-pill" placeholder="Type your password" type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>

                {
                    (isLoginActive) ?
                    <Button type="submit" className="rounded-pill login-button mt-5" variant="outline-success" block><span>LOGIN</span></Button> 

                    :

                    <Button type="submit" className="rounded-pill login-button mt-5" variant="outline-success" block disabled><span>LOGIN</span></Button>
                }
            </Form>

            <p className="text-center pt-3">or</p>

            <div className="text-center pb-5">
                <GoogleLogin
                    clientId = '1009218218792-e40pnhmnualf3vbq465jkfmnj74cgm3b.apps.googleusercontent.com'
                    render={renderProps => (
                        <img className="google-button" src={require('../images/btn_google_signin_light_normal_web@2x.png')} alt="" onClick={renderProps.onClick} disabled={renderProps.disabled}/>
                        
                    )}
                    buttonText="Login"
                    onSuccess={captureLoginResponse}
                    onFailure={captureLoginResponse}
                    cookiePolicy={'single_host_origin'}
                />
            </div>

            <CreateUser />
        </div>
    )
}