import React, {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import UserContext from '../contexts/userContext';

export const NavBar = () => {
    const {user} = useContext(UserContext);

    return (
        <>
            <Navbar expand="lg">
                <Navbar.Brand className="ml-2" href="/">
                    <div className="div-logo">
                        <img className="nav-logo" src={require('../images/connetone.png')} alt=""/>
                    </div>
                </Navbar.Brand>

                {
                    (user.id !== null) ?

                    <>
                        <Navbar.Toggle aria-controls='basic-nav-bar' />

                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto"></Nav>
                            <Nav>
                            <Nav.Link href="/Logout">{user.firstName}, Log out</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </>

                    :

                    null
                }
            </Navbar>
        </>
    )
}