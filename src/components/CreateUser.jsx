import React, {useState, useEffect} from 'react';
// import {useHistory} from 'react-router-dom';
import {Form, Button, Modal} from 'react-bootstrap';

export const CreateUser = () => {
    const [show, setShow] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isRegisterActive, setIsRegisterActive] = useState(false);

    // let history = useHistory();

    useEffect(() => {
        if(firstName !== '' && lastName !== '' && email !== '' && password !== '' && password.length > 7 && password === confirmPassword) return setIsRegisterActive(true);

        else return setIsRegisterActive(false);
    }, [firstName, lastName, email, password, confirmPassword])

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    const createAccount = e => {
        e.preventDefault();

        fetch('https://aqueous-shelf-56933.herokuapp.com/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            })
        }).then(res => res.json())
        .then(data => {
            if(data === true) {
                handleClose();
                alert("User registration successful!");
                //history.push('/');
            }

            else alert("Error in user registration!");
        })
    }

    return (
        <>
            <p className="text-center create-account-p pb-5">Don't have an account? <span className="text-primary create-account-span" onClick={handleShow}>Create Account</span></p>

            <Modal show={show} onHide={handleClose} size="md" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Create an account</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createAccount(e)}>
                        <Form.Group>
                            <Form.Label className="pl-2">First Name</Form.Label>

                            <Form.Control className="rounded-pill" type="text" placeholder="Juan" name="firstName" value={firstName} onChange={e => setFirstName(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label className="pl-2">Last Name</Form.Label>
                            
                            <Form.Control className="rounded-pill" type="text" placeholder="Dela Cruz" name="lastName" value={lastName} onChange={e => setLastName(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label className="pl-2">Email</Form.Label>
                            
                            <Form.Control className="rounded-pill" type="email" placeholder="juandelacruz@email.com" name="email" value={email} onChange={e => setEmail(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label className="pl-2">Password</Form.Label>
                            
                            <Form.Control className="rounded-pill" type="password" placeholder="" name="password" value={password} onChange={e => setPassword(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label className="pl-2">Confirm Password</Form.Label>

                            <Form.Control className="rounded-pill" type="password" placeholder="" name="confirmPassword" value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)} />
                            {
                                (password.length <= 7 && confirmPassword.length <= 7  && password !== '') ?
                                
                                <Form.Text className="pl-2 text-danger">Password must be 8 characters long!</Form.Text>

                                :

                                null
                            }

                            {
                                (password === confirmPassword) ?
                                (password !== '') ?
                                <Form.Text className="pl-2 text-success">Password matched succesfully!</Form.Text> : null

                                :

                                <Form.Text className="pl-2 text-danger">Password does not match!</Form.Text>
                            }
                        </Form.Group>

                        {
                            (isRegisterActive) ?

                            <Button type="submit" className="rounded-pill login-button my-5" variant="outline-sucess" block><span>Register</span></Button>

                            :

                            <Button type="submit" className="rounded-pill login-button my-5" variant="outline-sucess" block disabled><span>Register</span></Button>

                        }
                        
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    )
}
