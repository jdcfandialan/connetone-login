import React, {useEffect, useContext} from 'react';
import {Redirect} from 'react-router-dom';
import UserContext from '../contexts/userContext';

export const Logout = () => {
    const {unsetUser} = useContext(UserContext);

    useEffect(() => {
        unsetUser();
        // eslint-disable-next-line
    }, []);

    return (
        <Redirect to="/" />
    )
}