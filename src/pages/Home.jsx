import React, {useContext, useState} from 'react';
import  {Button} from 'react-bootstrap'
import UserContext from '../contexts/userContext';

export const Home = () => {
    const {user} = useContext(UserContext);

    const [language, setLanguage] = useState("English");

    const translate = () => {
        if(language === 'English') setLanguage('Not English');

        else setLanguage('English');
    }

    return (
        <>
            <h1 className="p-5">Hello {user.firstName}!</h1>

            <Button onClick={translate}>TRANSLATE</Button>

            {
                (language === 'English') ? 

                <p>This text is in english</p>

                :

                <p>hindi ito english</p>
            }
        </>
    )
}