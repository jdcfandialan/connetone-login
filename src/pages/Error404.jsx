import React from 'react';
import {Link} from 'react-router-dom';

export const Error404 = () => {
    return (
        <div className="d-flex flex-column align-items-center justify-content-center vh-100">
            <h1 style={{fontWeight: 'bold'}}>Sorry, this page isn't available.</h1>
            <h6 className="pt-5">The link to the page might be broken or the page does not exist.
                <span>
                    <Link to={'/'}> Return to Home page.</Link>
                </span>
            </h6>
        </div>
    )
}