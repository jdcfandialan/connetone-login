import React, {useState, useEffect} from 'react';
import {UserProvider} from './contexts/userContext';
import './App.css';

import {NavBar} from './components/NavBar';
import {renderRoutes} from './router/index';

export default function App() {
    const [user, setUser] = useState({id: null, firstName: ''});

    const unsetUser = () => {
        localStorage.setItem('token', undefined);
        localStorage.clear();

        setUser({
            id: null,
            firstName: ''
        })
    }

    useEffect(() => {
        fetch('https://aqueous-shelf-56933.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json())
        .then(data => {
            if(data._id){
                setUser({
                    id: data._id,
                    firstName: data.firstName
                })
            }

            else return {id: null, firstName: ''}
            
        });
    }, [user.id])
    
    return (
        <>
            <UserProvider value={{user, setUser, unsetUser}}>
                <NavBar />
                {
                    renderRoutes()
                }
            </UserProvider>
        </>
    )
}
