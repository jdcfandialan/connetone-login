import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {routes} from './routes';

export const renderRoutes = () => {
    return (
        <BrowserRouter>
            <Switch>
                {
                    routes.map(route => (
                        <Route key={route.path} {...route} />
                    ))
                }
            </Switch>
        </BrowserRouter>
    )
}