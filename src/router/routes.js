import {Index} from '../pages/Index';
import {Home} from '../pages/Home';
import {Logout} from '../pages/Logout';
import {Error404} from '../pages/Error404';

export const routes = [
    {
        name: 'Index',
        path: '/',
        exact: true,
        component: Index
    },

    {
        name: 'Home',
        path: '/home',
        exact: true,
        component: Home
    },

    {
        name: 'Logout',
        path: '/logout',
        exact: true,
        component: Logout
    },

    {
        name: 'Error404',
        path: '*',
        exact: false,
        component: Error404
    }
]